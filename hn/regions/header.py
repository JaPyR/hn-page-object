from pypom import Region
from selenium.webdriver.common.by import By
from hn.pages.new import HNNew


class Header(Region):

    _root_locator = (By.XPATH, '//table[@id="hnmain"]/tbody/tr[1]')
    _new_locator = (By.XPATH, '//a[text()="new"]')
    _comments_locator = (By.XPATH, '//a[text()="comments"]')

    def open_new(self):
        element = self.find_element(*self._new_locator)
        element.click()
        return HNNew(self.driver).wait_for_page_to_load()
