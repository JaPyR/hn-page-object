from pypom import Page
from selenium.webdriver.common.by import By
from hn.regions.header import Header


class BasePage(Page):

    _main_view_locator = (By.XPATH, '//table[@id="hnmain"]/tbody/tr[3]')
    _more_linl_locator = (By.XPATH, '//a[@class="morelink"]')

    @property
    def loaded(self):
        return self.is_element_displayed(*self._main_view_locator)

    @property
    def header(self):
        return Header(self)

    def load_more(self):
        element = self.find_element(*self._more_linl_locator)
        element.click()
        return self.wait_for_page_to_load()
