import time
from selenium import webdriver
from hn.pages.base import BasePage

driver = webdriver.Chrome()
driver.maximize_window()
page = BasePage(driver, 'https://news.ycombinator.com/').open()
new = page.header.open_new()

time.sleep(3)
driver.quit()
